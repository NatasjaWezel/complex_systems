# Self Organized Criticality in Code Repositories
Project for the Complex System Simulation course in the UvA Master Computational Science.

## Why Power Laws? An Explanation from Fine-Grained Code Changes
Abstract Syntax Tree 

Evolution of Software

Fitness of methods -> Uniform (random) distribution

### Creating

### Calling
Take two functions and link them

Question: how are these selected? Via something preferential?
Answer: method S is chosen as the caller with probability proportional to the size of its body (number of statements).

### Updating

### Deleting

#### Ideas
Code decay -> fitness decrease over time?
Question: why?
